Este es un pequeño programa que corre en terminal para obtener el código ASCII de un caracter introducido.
Lo he hecho pensando en que sea usado en Linux, pero como está programado en ANSI C puede funcionar en otras plataformas.
