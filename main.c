#include <stdio.h>
#include <ctype.h>

void flush() {
    int c;
    while((c = getchar()) != '\n' && c != EOF);
}

int main() {
    int a; char comp;
    
    while(comp != 's')
    {
        printf("Introduce un caracter y presiona ENTER: ");
        a = getchar();
        flush();
        printf("El código en ASCII es: %d", a);
        printf("\n¿Quieres salir? s/n: ");
        comp = tolower(getchar());
        flush();
    }
    
    return 0;
}
    
    
